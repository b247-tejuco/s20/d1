//[SECTION] While Loop
	//- A while loop takes in an expression/condition

	let count = 5;

	while(count !== 0){
		console.log('While: '+count);
		count --;
	};

//Another simple example

let grade= 1;
while(grade <= 5){
	console.log("I am a grade "+grade +" student!");
	//withouth he 2nd statement, this wil cr8 infiniteloop

	grade ++;
};	//thus, puitting 2nd statement is necessary in preventing
//infinite loop

/*

Syntax:
	while(expression/condition){
		statement
	}

*/

	//Another example
		let a= 1;
		let b= 5;

		while(a <= b){
			console.log('Apple '+ a);
			a++;
		};
//[SECTION] Do While Loop
	//- A do-while loop works a lot like the while loop.
	// But unlike while loops,
	//do-while loops guarantee that the code will be executed atleast once.

	/*
		Syntax:
			do{
				statement
			}while(expression/condition)
	*/

	let number= Number(prompt('number(prompt)Give me a number'));
	console.log(number);
		console.log(typeof number);

		do{
			console.log('Do While: '+number);
			number += 2;
			/*++ increment limited to 1 while 
			+= addition ass. can choose how many increment*/
		}while (number < 10);

			//Another Example

			let c= 1;
			let d= 5;

				do{
					console.log('Carrots '+ c);
					c++;

				} while(c <= d);

	//[SECTION] For Loop
		//- A for loop is more flexibe that while and do-while loops.
		// It consists of three parts:
			// 1. The 'initialization' value that will track the progression of the loop
			// 2. The 'expression/condition' that will be evaluated which will
			//determined whether the loop will run one omre tiumee.
			//3. The 'finalExpression' indicates how to advance the loop.

				let f= 5;

				for(let e= 1; e <= f; e++){
					console.log('Eggplant '+ e)
				};

		/*
		Syntax:
			for(initialization; expression/condition; finalExpression){
				statement;
			}
		*/

			for(let count= 0; count <= 10; count+= 3){
				console.log(count);
			};

		let myStrings= 'Voren';
		console.log(myStrings);
		console.log(myStrings.length);

			console.log(myStrings[0]);
			console.log(myStrings[1]);
			console.log(myStrings[2]);
			
		for(let x= 0; x < myStrings.length; x++){
			//the current value of myStrings is printed out using 
			//its index value
			console.log(myStrings[x])

		};

		//Create a string named 'myName' with a value of your name;

		let myName= 'Antetokounmpo';
		for(let i= 0; i < myName.length; i++){
			if (
				myName[i].toLowerCase()== 'a' ||
				myName[i].toLowerCase()== 'e' ||
				myName[i].toLowerCase()== 'i' ||
				myName[i].toLowerCase()== 'o' ||
				myName[i].toLowerCase()== 'u' 

				){
					console.log(3);

			} else {
				console.log(myName[i])
			}
		};

	//[SECTION] Continue and Break Statements

		//-The 'Continue ' statement allows the code to go to the next
		//iteration of the loop withiout finishing the execution of all 
		//statements in a code block.

		// The break' statements is used to terminate the current loop
		//on ce a match has been found.

			//simple example of break

			for (let g= 1; g <= 12; g++){
				if (g === 6)
					break;

				console.log('Grapes '+ g);
			};

		//simple example of conntinue

		for(let i= 1; i <= 12; i++){
			if(i === 6)
				continue;

			console.log('Indian Mango '+ i)
		};


		//example 2 continue + break usually partners 
		for(let count= 0; count <= 20; count++){
			if(count % 2 === 0) {
				continue;
			}

			console.log('Continue and Break: '+count);

			//if the current value of count is greater than 13
			if(count > 13){

				//tells the code to terminate/stop the loop even if
				//the expression/condition of the loop defines
				//that it should execute so long as the value
				//of count is less than or equal to 20
				break;
			}
		};
	let name= 'alejandro';

		for(let i= 0; i < name.length; i++){
			console.log(name[i]);

			//if the vowel is equal to a, continue to the next iteration of
			//the loop
			if(name[i].toLowerCase() === 'a'){
				console.log('Continue to the next iteration');
				continue;
			}

			if(name[i].toLowerCase() == 'd'){
				break;
			}

		}